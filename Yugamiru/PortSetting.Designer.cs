﻿namespace Yugamiru
{
    partial class PortSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.IDC_OK = new System.Windows.Forms.Button();
            this.IDC_Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "PORT";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(122, 38);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 1;
            // 
            // IDC_OK
            // 
            this.IDC_OK.Location = new System.Drawing.Point(32, 91);
            this.IDC_OK.Name = "IDC_OK";
            this.IDC_OK.Size = new System.Drawing.Size(75, 23);
            this.IDC_OK.TabIndex = 2;
            this.IDC_OK.Text = "OK";
            this.IDC_OK.UseVisualStyleBackColor = true;
            this.IDC_OK.Click += new System.EventHandler(this.IDC_OK_Click);
            // 
            // IDC_Cancel
            // 
            this.IDC_Cancel.Location = new System.Drawing.Point(122, 91);
            this.IDC_Cancel.Name = "IDC_Cancel";
            this.IDC_Cancel.Size = new System.Drawing.Size(75, 23);
            this.IDC_Cancel.TabIndex = 3;
            this.IDC_Cancel.Text = "CANCEL";
            this.IDC_Cancel.UseVisualStyleBackColor = true;
            this.IDC_Cancel.Click += new System.EventHandler(this.IDC_Cancel_Click);
            // 
            // PortSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(254, 122);
            this.Controls.Add(this.IDC_Cancel);
            this.Controls.Add(this.IDC_OK);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PortSetting";
            this.Text = "PORT SETTINGS";
            this.Load += new System.EventHandler(this.PortSetting_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button IDC_OK;
        private System.Windows.Forms.Button IDC_Cancel;
    }
}