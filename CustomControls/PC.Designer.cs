﻿namespace CustomControls
{
    partial class PC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.chkSelected = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtInsName = new System.Windows.Forms.Label();
            this.txtPCName = new System.Windows.Forms.Label();
            this.txtInstallDate = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.picDisbPC = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picDisbPC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // chkSelected
            // 
            this.chkSelected.AutoSize = true;
            this.chkSelected.Location = new System.Drawing.Point(2, 3);
            this.chkSelected.Name = "chkSelected";
            this.chkSelected.Size = new System.Drawing.Size(15, 14);
            this.chkSelected.TabIndex = 0;
            this.chkSelected.UseVisualStyleBackColor = true;
            this.chkSelected.CheckedChanged += new System.EventHandler(this.chkSelected_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(127, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Installation Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(127, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "PC Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(127, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Installation Date";
            // 
            // txtInsName
            // 
            this.txtInsName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtInsName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInsName.Location = new System.Drawing.Point(218, 3);
            this.txtInsName.Name = "txtInsName";
            this.txtInsName.Size = new System.Drawing.Size(143, 20);
            this.txtInsName.TabIndex = 5;
            this.txtInsName.TextChanged += new System.EventHandler(this.txtInsName_TextChanged);
            // 
            // txtPCName
            // 
            this.txtPCName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtPCName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPCName.Location = new System.Drawing.Point(218, 27);
            this.txtPCName.Name = "txtPCName";
            this.txtPCName.Size = new System.Drawing.Size(143, 20);
            this.txtPCName.TabIndex = 6;
            this.txtPCName.TextChanged += new System.EventHandler(this.txtPCName_TextChanged);
            // 
            // txtInstallDate
            // 
            this.txtInstallDate.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtInstallDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInstallDate.Location = new System.Drawing.Point(218, 52);
            this.txtInstallDate.Name = "txtInstallDate";
            this.txtInstallDate.Size = new System.Drawing.Size(143, 20);
            this.txtInstallDate.TabIndex = 7;
            this.txtInstallDate.TextChanged += new System.EventHandler(this.txtInstallDate_TextChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // picDisbPC
            // 
            this.picDisbPC.Image = global::CustomControls.Properties.Resources.disPC;
            this.picDisbPC.Location = new System.Drawing.Point(6, -1);
            this.picDisbPC.Name = "picDisbPC";
            this.picDisbPC.Size = new System.Drawing.Size(121, 77);
            this.picDisbPC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDisbPC.TabIndex = 8;
            this.picDisbPC.TabStop = false;
            this.picDisbPC.Click += new System.EventHandler(this.picDisbPC_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CustomControls.Properties.Resources.PC_Icon;
            this.pictureBox1.Location = new System.Drawing.Point(6, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(121, 77);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.picDisbPC_Click);
            // 
            // PC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkSelected);
            this.Controls.Add(this.picDisbPC);
            this.Controls.Add(this.txtInstallDate);
            this.Controls.Add(this.txtPCName);
            this.Controls.Add(this.txtInsName);
            this.Controls.Add(this.pictureBox1);
            this.Name = "PC";
            this.Size = new System.Drawing.Size(364, 77);
            this.EnabledChanged += new System.EventHandler(this.PC_EnabledChanged);
            ((System.ComponentModel.ISupportInitialize)(this.picDisbPC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.CheckBox chkSelected;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label txtInsName;
        public System.Windows.Forms.Label txtPCName;
        public System.Windows.Forms.Label txtInstallDate;
        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.PictureBox picDisbPC;
    }
}
