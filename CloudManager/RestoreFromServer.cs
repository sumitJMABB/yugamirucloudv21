﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CloudManager
{
    /// <summary>
    /// This will initiate the datarestoration in local system. This will be called very rarely even in most cases never.
    /// </summary>
    public static class RestoreFromServer
    {
        public static void Start()
        {
            //Testing
            //frmCustomSyncSettingParams frm = new frmCustomSyncSettingParams();
            //syncSettingsMaster frm = new syncSettingsMaster();
            //frm.ShowDialog();
            //return;
            //CloudSyncManager.SynchronizeAll();

            //Testing



            bool insertStatus = false;
            //start infinite loop for receiving unknown amount of data. and break it once done.
            string server_user_id = "0";
            for (int uniqueId=1; true; uniqueId++)
            {

                //-------------Added By Sumit on 31-Dec-18 for GSP-983 START-------------
                if (uniqueId != 1)
                {
                    //it will fetch the last max avalable User_ID field of web server we are storing as server_user_id in local DB
                    server_user_id = GlobalItems.Get_Max_Server_User_ID(GlobalItems.MY_Reg_User_ID, GlobalItems.MY_StallID);
                }
                //-------------Added By Sumit on 31-Dec-18 for GSP-983 END---------------


                ////===First table patientdetails.
                string bsonPatient = GetBsonFromServer.getBsonFromServer(GetBsonFromServer.initReqPatientJson, uniqueId.ToString(), CloudUrls.PatientDetailsGetFromUrl,server_user_id);

                //================Edited by sumit for GSP-983 START============
                //if(bsonPatient.Contains("Data Doesnot exist"))
                //{
                //    break;
                //}
                if (bsonPatient.Contains("Data Doesnot exist") || bsonPatient.Contains("Failed due to Insufficient Data"))
                {
                    break;
                }
                //================Edited by sumit for GSP-983 END============



                if (bsonPatient.Length > 0)//--Added by Rajnish for GSP-676--//
                {
                    DataTable dtPat = BsonParser.SpliterPatientDetails(bsonPatient);//PatientDetailsBSONtoDT(bsonPatient);

                    //Added by Sumit on 06-Dec-2018 GSP-944---------------START
                    try
                    {
                        foreach (DataRow dr in dtPat.Rows)
                        {
                            //Added by Sumit GSP-1017---------START
                            //PatientID                           


                            //PatientId column
                            dr["PatientId"] = SplCharHandler.GetNormalString(dr["PatientId"].ToString());
                            if (dr["PatientId"].ToString().Contains("'"))
                            {
                                dr["PatientId"] = dr["PatientId"].ToString().Replace("'", @"''");
                            }
                            //Name column
                            dr["Name"] = SplCharHandler.GetNormalString(dr["Name"].ToString());
                            if (dr["Name"].ToString().Contains("'"))
                            {
                                dr["Name"] = dr["Name"].ToString().Replace("'", @"''");
                            }
                            //Comment Column


                //Added by Sumit GSP-1017---------START
                            //PatientID
                            //Name
                            //comment
                            dr["Comment"] = SplCharHandler.GetNormalString(dr["Comment"].ToString());
                            string cmt = dr["Comment"].ToString();
                            string[] splter = { "xSTARTxcxoxmxmxexnxtx", "xENDxcxoxmxmxexnxtx" };
                            List<string> lstCmtParts = new List<string>(cmt.Split(splter, StringSplitOptions.RemoveEmptyEntries));
                            if(lstCmtParts.Count>=3)
                            {
                                dr["Comment"] = lstCmtParts[lstCmtParts.Count-2];
                            }
                //Added by Sumit GSP-1017---------END
                            if (dr["Comment"].ToString().Contains("'"))
                            {
                                dr["Comment"] = dr["Comment"].ToString().Replace("'", @"''");
                            }
                        }
                    }
                    catch (Exception e1)
                    { }
                    //Added by Sumit on 06-Dec-2018 GSP-944---------------END


                    //-------------Added By Sumit on 28-Dec-18 for GSP-983 START-------------
                    
                    for(int clNum=0;clNum<dtPat.Columns.Count;clNum++)
                    {//Sumit: Code can be improved further, loop can be removed
                        if(dtPat.Columns[clNum].ColumnName.ToUpper()=="USER_ID")
                        {
                            dtPat.Columns[clNum].ColumnName = "SERVER_USER_ID";
                            break;
                        }
                    }
                    //-------------Added By Sumit on 28-Dec-18 for GSP-983 END---------------

                    string pdqr = DBHandler.QryConverter(dtPat, Tables.PatientDetails.ToString());
                    ////===insert into database 
                   
                    insertStatus = DBHandler.ExecQry(pdqr, uniqueId.ToString(), Tables.PatientDetails);

                    try
                    {
                        // if(insertStatus)
                        {
                            string bsonkneedown = GetBsonFromServer.getBsonFromServer(GetBsonFromServer.initReqKneeDownJson, uniqueId.ToString(), CloudUrls.FrontBodyPositionKneeDownGetFromUrl, server_user_id);
                            DataTable dtknee = BsonParser.SpliterFrontBodyPositionKneedown(bsonkneedown);
                            ////===convert table to qry
                            string q = DBHandler.QryConverter(dtknee, Tables.FrontBodyPositionKneeDown.ToString());
                            ////===insert into database 
                            insertStatus = DBHandler.ExecQry(q, uniqueId.ToString(), Tables.FrontBodyPositionKneeDown);


                            string bsonstanding = GetBsonFromServer.getBsonFromServer(GetBsonFromServer.initReqStandingJson, uniqueId.ToString(), CloudUrls.FrontBodyPositionStandingGetFromUrl, server_user_id);
                            //DataTable dtStanding = BsonParser.ColumnNamesFrontBodyPositionStanding(bsonstanding);
                            DataTable dtStanding = BsonParser.SpliterFrontBodyPositionStanding(bsonstanding);
                            string r = DBHandler.QryConverter(dtStanding, Tables.FrontBodyPositionStanding.ToString());
                            //insert into database 
                            //**************
                            insertStatus = DBHandler.ExecQry(r, uniqueId.ToString(), Tables.FrontBodyPositionStanding);//**************


                            string bsonSideBody = GetBsonFromServer.getBsonFromServer(GetBsonFromServer.initReqsideJson, uniqueId.ToString(), CloudUrls.SideBodyPositionGetFromUrl, server_user_id);
                            //DataTable dtSideBody = BsonParser.PatientDetailsBSONtoDT(bsonSideBody);
                            DataTable dtSideBody = BsonParser.SpliterSideBodyPosition(bsonSideBody);
                            string s = DBHandler.QryConverter(dtSideBody, Tables.SideBodyPosition.ToString());
                            //insert into database 
                            insertStatus = DBHandler.ExecQry(s, uniqueId.ToString(), Tables.SideBodyPosition);

                        }
                    }
                    catch(Exception e2)
                    { }
                }
                //else
                //{
                //    throw new Exception("sqlite db insert/update error");
                //}
                //Save 
            }
        }


    }
}
