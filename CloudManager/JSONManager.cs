﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.Data.SQLite;



namespace CloudManager
{
    public static class JSONManager
    {
        public static string getJSONToSend(Tables tableName = Tables.PatientDetails)
        {
            string jsonContent = (new ConvertTableToJSON((new AppDataReader()).ReadTableData(tableName), tableName)).GetJSONPatientDetails();

            return jsonContent;
        }

        public static bool JsonTOBsonSender(string json, string url, string uniqueid="")//default parameter "uniqueid" added by sumit GSP-983
        {
            //============Testing for JSON direct save to file
            //string filename = "";
            //if(json.Contains("KneeDown"))
            //{
            //    filename = "kneecoding.txt";
            //}
            //else if (json.Contains("Standing"))
            //{
            //    filename = "standingcoding.txt";
            //}
            //else if (json.Contains("SideBody"))
            //{
            //    filename = "siedbodycoding.txt";
            //}
            //if(filename!="")
            //{
            //    File.WriteAllText(filename, json);
            //}
            //===============================================
            bool isSuccess = false;
            try
            {
                String queryString = json;

                //queryString = queryString.Replace("\\", @"\\");//Added by Rajnish For GSP-901//
                //queryString = queryString.Replace("\\", @"\\").Replace("\"\"", "\\\"");//Added by Rajnish For GSP-901//   --commented by sumit due to GSP-944             

                #region Sample Query JSON

                //==================================================
                //queryString = "[{\"Computer_id\": \"BFEBFBFF000406E4\",\"Computer_name\": \"DESKTOP-9QLKVGO\",\"Activation_key\": \"BWHP0T0A00AG712J8Q8Z391HUG8KTH6\"}]";

                //=====================================================
                #endregion

                //commented by Rajnish For GSP-901//
                //var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(JsonConvert.ToString(queryString));// queryString;//Newtonsoft.Json.JsonConvert.DeserializeObject(@"[{""TableName"":""PatientDetails"",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""1""}]");//queryString);
                //3.
                //Added by Rajnish For GSP-901//
                //JsonSerializerSettings settings = new JsonSerializerSettings
                //{
                //    StringEscapeHandling = StringEscapeHandling.EscapeHtml
                //};

                //var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString, settings);--commented by sumit GSP-944
                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //----------------------------//
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);

                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);

                #region sample Json
                //data = @"[{""TableName"":""PatientDetails"",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""1""}]";

                //================================================================================
                #endregion


                byte[] requestByte = objBsonMemoryStream.ToArray();
                WebRequest webRequest = WebRequest.Create(url);     //(@"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/downloadpatient_data");//CloudUrls.FrontBodyPositionKneeDownSendToUrl);
                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = requestByte.Length;

                // create our stram to send
                Stream webDataStream = null;
                try
                {
                    webDataStream = webRequest.GetRequestStream();
                    webDataStream.Write(requestByte, 0, requestByte.Length);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                //webResponse.ContentType = "text/html; charset=ASCII";
                string tp = webResponse.ContentType;
                try
                {
                    webDataStream = webResponse.GetResponseStream();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);
                String responseFromServer = webResponseSReader.ReadToEnd();
                isSuccess = true;
                //=========================================================
                //-------------Added By Sumit on 28-Dec-18 for GSP-983 START-------------
                //Parsing
                if (json.Contains("PatientDetails") && uniqueid.Trim()!="")
                {
                    DataTable dtRemoverStrs = CloudManager.InstallInfoManager.GetRemoverStringsTable();
                    string[] strResCols = { "user_id",
                                        "reg_user_id"};
                    List<string> lstColVals = new List<string>(responseFromServer.Split(strResCols, StringSplitOptions.None));
                    string server_user_id = string.Empty;

                    if (lstColVals.Count == 3)
                    {
                        server_user_id = lstColVals[1];
                    }

                    foreach (DataRow dr in dtRemoverStrs.Rows)
                    {
                        server_user_id = server_user_id.Replace(dr[0].ToString(), "");
                    }
                    int tempsuid;
                    if (int.TryParse(server_user_id, out tempsuid))
                    {
                        //Update database
                        SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
                        SQLiteCommand cmd;
                        sqlite.Open();  //Initiate connection to the db
                        cmd = sqlite.CreateCommand();
                        string qry = "";
                        qry += "update PatientDetails set Server_User_Id='" + server_user_id + "' where uniqueid=" + uniqueid;                        
                        cmd.CommandText = qry;
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch(SQLiteException dbex)
                        {
                            throw dbex;
                        }
                        sqlite.Close();                        
                    }
                    else
                    {
                        throw new Exception("PatientDetails: Parsing Error-User_id = " + server_user_id);
                    }
                }
                //-------------Added By Sumit on 28-Dec-18 for GSP-983 END---------------
                //=========================================================
            }
            catch (Exception ex)
            {
                //Added by Sumit GSP-1260---START
                //MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
                //Added by Sumit GSP-1260---END
                isSuccess = false;
            }

            #region commented
            //Encoding ad = webResponseSReader.CurrentEncoding;
            //string v1 = responseFromServer.Replace(@"\u", "=");
            ////byte[] arb = Convert.ToByte(responseFromServer);
            ////string sd = Convert.ToBase64String( 
            //string input = responseFromServer;
            //var utf8bytes = Encoding.UTF8.GetBytes(input);
            //var win1252Bytes = Encoding.Convert(Encoding.UTF8, Encoding.ASCII, utf8bytes);
            //string s_unicode2 = System.Text.Encoding.UTF8.GetString(win1252Bytes);
            //Encoding utf8 = Encoding.UTF8;
            //Encoding ascii = Encoding.ASCII;            
            //string output = ascii.GetString(Encoding.Convert(utf8, ascii, utf8.GetBytes(input)));


            ////var a1[] = ASCIIEncoding.

            //string val2 = responseFromServer.Replace("\\u", "*");
            //string val3 = val2.Replace(@"\u", ":");
            //string charat = @"\u";
            //string AnsRes = (new UriTypeConverter()).ConvertToInvariantString(responseFromServer);
            //string AnsRes1 = (new UriTypeConverter()).ConvertToString(responseFromServer);
            //AnsRes1 = AnsRes1.Replace("\0", "").Replace("�", "-");//
            #endregion
            return isSuccess;
        }

        public static string SendJSON_StrToUrl(string strJsonToSend, string url)
        {
            String queryString = strJsonToSend;
            var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
            JsonSerializer jsonSerializer = new JsonSerializer();
            MemoryStream objBsonMemoryStream = new MemoryStream();
            BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
            jsonSerializer.Serialize(bsonWriterObject, studentObject);

            byte[] requestByte = objBsonMemoryStream.ToArray();
            WebRequest webRequest = WebRequest.Create("http://52.197.210.82/apioauthdata/index.php/cloudData/getupdate_counter");
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.ContentLength = requestByte.Length;

            // create our stram to send
            Stream webDataStream = null;
            try
            {
                webDataStream = webRequest.GetRequestStream();
                webDataStream.Write(requestByte, 0, requestByte.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            string ed = webDataStream.ToString();
            // get the response from our stream
            WebResponse webResponse = webRequest.GetResponse();
            //webResponse.ContentType = "text/html; charset=ASCII";
            string tp = webResponse.ContentType;
            try
            {
                webDataStream = webResponse.GetResponseStream();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // convert the result into a String
            StreamReader webResponseSReader = new StreamReader(webDataStream);
            String responseFromServer = webResponseSReader.ReadToEnd();

            return responseFromServer;
        }
    }
}
