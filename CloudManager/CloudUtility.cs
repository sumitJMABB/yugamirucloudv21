﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace CloudManager
{
    class CloudUtility
    {
        /// <summary>
        /// Get most latest (LUT) Last Updated Timestamp
        /// </summary>
        /// <returns></returns>
        public static Cloud.PatientRecord MaxLatest(string sqliteDBPath=@"C:\ProgramData\gsport\Yugamiru cloud\database\yugamiru.sqlite", string tableName="PatientDetails")
        {
            Cloud.PatientRecord retVal = new Cloud.PatientRecord();
            //sqliteDBPath = "cpdb.sqlite";
            SQLiteConnection AppDBCon = new SQLiteConnection("Data Source=" + sqliteDBPath);
            try
            {
                SQLiteCommand cmd;
                AppDBCon.Open();  //Initiate connection to the db
                cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
                cmd.CommandText = "select uniqueid,lut from PatientDetails;";
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                DataTable dt = new DataTable();
                ad.Fill(dt); //fill the datasource
                //close connection
                AppDBCon.Close();
                List<Cloud.PatientRecord> patientRecords = new List<Cloud.PatientRecord>();
                foreach(DataRow dr in dt.Rows)
                {
                    Cloud.PatientRecord pr = new Cloud.PatientRecord(dr);                    
                    patientRecords.Add(pr);
                }
                Cloud.PatientRecord tempPr = null;
                int cnt = 0;
                foreach(Cloud.PatientRecord lpr in patientRecords)
                {
                    cnt++;
                    if(cnt==1 )
                    {
                        tempPr = lpr;
                    }
                    else if(lpr.LUT==tempPr.LUT ||  lpr.LUT>tempPr.LUT)
                    {
                        tempPr = lpr;
                    }
                }
                retVal = tempPr;             
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
                // MessageBox.Show(ex.Message);
                throw ex;
            }
            catch (Exception ex)
            {
                //Add your exception code here.
                // MessageBox.Show(ex.Message);
                throw ex;
            }

            return retVal;
        }

        public static Dictionary<string, string> SetCloudSetting()
        {
            Dictionary<string, string> KeyConfig = new Dictionary<string, string>();
            DataTable dt = new DataTable();
            SQLiteDataAdapter ad;
            SQLiteConnection sqlite;
            try
            {
                sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = "select * from KeyConfig";  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
                sqlite.Close();

                KeyConfig.Add("Manual", "FALSE");
                KeyConfig.Add("Automatic", "FALSE");
                KeyConfig.Add("Custom", "FALSE");
                KeyConfig.Add("TakeLatest_Before_Open", "FALSE");
                KeyConfig.Add("Block_Edit_NonLocal_Reports", "FALSE");
                KeyConfig.Add("EditAndSave_As_New", "FALSE");
                KeyConfig.Add("Offilne_Block_Edit_NonLocal_Reports", "FALSE");
                KeyConfig.Add("Offline_Allow_Edit_But_Save_As_New", "FALSE");

                KeyConfig.Add("If_Application_Is_Idle_For_More_Than_Minute", "FALSE");
                KeyConfig.Add("Between_From_To_Date", "FALSE");
                KeyConfig.Add("Closing_Application", "FALSE");
                KeyConfig.Add("Starting_Application", "FALSE");

                string qry = "";
                foreach (DataRow dr in dt.Rows)
                {
                    foreach (KeyValuePair<string, string> column in KeyConfig)
                    {
                        if (dr["KeyName"].ToString() == column.Key.ToString())
                        {
                            KeyConfig.Remove(column.Key);
                            //string[] parts = dr["ActiveStatus"].ToString().Split('_');
                            KeyConfig.Add(dr["KeyName"].ToString(), dr["ActiveStatus"].ToString());
                            break;
                        }
                    }
                }


            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
                //MessageBox.Show(ex.Message);
            }
            return KeyConfig;
        }
    }
}
