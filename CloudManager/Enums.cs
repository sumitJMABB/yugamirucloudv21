﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudManager
{    
   public enum Tables {None, PatientDetails, FrontBodyPositionKneeDown, FrontBodyPositionStanding, SideBodyPosition, tblInstallInfo };
    public enum RowNumToJSON { AllRows=0,FirstRow=1,LastRow=2};
    public enum SyncResults {None, Success, NoNewRecord, NoNetConnection, LocalDBError, ServerError, GenericFailure };
}
