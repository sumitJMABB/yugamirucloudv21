﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudManager
{
    public static class MyExtensionMethods
    {
        public static bool ToBool(this String s)
        {
            bool ret = true;
            string valToCheck = s.Split('_')[0];
            if(valToCheck.Trim().ToUpper()=="TRUE")
            {
                ret = true;
            }
            else if (valToCheck.Trim().ToUpper() == "FALSE")
            {
                ret = false;
            }
            else
            {
                throw new Exception(s + " is Not A bool type string value");
            }
            return ret;
        }
    }
}
