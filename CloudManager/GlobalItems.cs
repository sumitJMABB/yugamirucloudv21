﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace CloudManager
{
    public class GlobalItems
    {
        static string _ComputerID = string.Empty;
        static string _ActivationKey = string.Empty;
        static string _sqliteDBPath = initCloudSystem.db_file; //@"E:\GSPORT\SQLDBTest\cpdb.sqlite";
        public static bool SetValues(string computerID, string activationKey)
        {
            //_ActivationKey = "4444kgjyftyfjvjhg"; it is for testing
            //_ComputerID= "55555hgjyfjhg";         it is for testing

            if (_ActivationKey.Length == 0 && _ComputerID.Length == 0)
            {
                _ActivationKey = activationKey;
                _ComputerID = computerID;
            }
            return true;
        }
        public static string ActivationKey
        {
            get
            {
                if (_ActivationKey.Length == 0)
                {
                    throw new Exception("Activation key has not been initialized yet");
                }
                return _ActivationKey;
            }
        }
        public static string ComputerID
        {
            get
            {
                if (_ComputerID.Length == 0)
                {
                    throw new Exception("ComputerID has not been initialize yet");
                }
                return _ComputerID;
            }
        }
        public static string sqliteDBPath
        {
            get
            {
                return _sqliteDBPath;
            }
        }
        //-------------Property Added By Sumit on 30-Dec-18 for GSP-983
        static string _StallID = string.Empty;
        public static string MY_StallID
        {
            get
            {
                if (_StallID.Length == 0)
                {
                    try
                    {
                        SetInstallIDs();
                    }
                    catch
                    {
                        throw new Exception("Install ID has not been initialize yet");
                    }
                }
                return _StallID;
            }
        }
        //-------------Property Added By Sumit on 30-Dec-18 for GSP-983
        static string _reg_User_ID = string.Empty;
        public static string MY_Reg_User_ID
        {
            get
            {
                if (_reg_User_ID.Length == 0)
                {
                    try
                    {
                        SetInstallIDs();
                    }
                    catch
                    {
                        throw new Exception("Reg_User_ID has not been initialize yet");
                    }
                }
                return _reg_User_ID;
            }
        }
        public static string _Server_User_ID = string.Empty; //Added by Rajnish for GSP-1104
        public static string MY_Server_User_ID
        {
            get
            {
                if (_Server_User_ID.Length == 0 || _Server_User_ID == null)
                {
                    try
                    {
                        _Server_User_ID = "0";
                    }
                    catch
                    {
                        throw new Exception("Server_User_ID has not been initialize yet");
                    }
                }
                return _Server_User_ID;
            }
        }
        //-------------Method Added By Sumit on 30-Dec-18 for GSP-983
        private static void SetInstallIDs()
        {
            SQLiteConnection sqlite;
            DataTable dt = new DataTable();
            SQLiteDataAdapter ad;
            try
            {
                sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = "select Stall_ID, Reg_User_ID from tblInstallInfo where sync='THIS'";  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
                if (dt.Rows.Count == 1)
                {
                    _StallID = dt.Rows[0]["Stall_ID"].ToString();
                    _reg_User_ID = dt.Rows[0]["Reg_User_ID"].ToString();
                }
                sqlite.Close();

            }
            catch (SQLiteException ex)
            {
                throw ex;
                //Add your exception code here.
                //MessageBox.Show(ex.Message);
            }
        }
        public static void SetForignInstallIDs(string OwnerUniqueId)
        {
            SQLiteConnection sqlite;
            DataTable dt = new DataTable();
            SQLiteDataAdapter ad;
            try
            {
                sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = "select Stall_ID, Reg_User_ID, Server_User_ID from PatientDetails where Owners_Unique_ID = " + OwnerUniqueId;  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
                if (dt.Rows.Count == 1)
                {
                    _StallID = dt.Rows[0]["Stall_ID"].ToString();
                    _reg_User_ID = dt.Rows[0]["Reg_User_ID"].ToString();
                    _Server_User_ID = dt.Rows[0]["Server_User_ID"].ToString();

                }
                sqlite.Close();

            }
            catch (SQLiteException ex)
            {
                throw ex;
                //Add your exception code here.
                //MessageBox.Show(ex.Message);
            }
        }
        //-------------Method Added By Sumit on 30-Dec-18 for GSP-983
        public static string Get_Reg_User_ID(string Stall_ID)
        {
            SQLiteConnection sqlite;
            DataTable dt = new DataTable();
            SQLiteDataAdapter ad;
            string reg_User_ID = "";
            try
            {
                sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = "select Reg_User_ID from tblInstallInfo where Stall_ID='" + Stall_ID + "'";  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource

                if (dt.Rows.Count == 1)
                {
                    reg_User_ID = dt.Rows[0]["Reg_User_ID"].ToString();
                }
                sqlite.Close();

            }
            catch (SQLiteException ex)
            {
                throw ex;
                //Add your exception code here.
                //MessageBox.Show(ex.Message);
            }

            return reg_User_ID;
        }

        //-------------Method Added By Sumit on 30-Dec-18 for GSP-983
        public static string Get_Stall_ID(string Reg_User_ID)
        {
            SQLiteConnection sqlite;
            DataTable dt = new DataTable();
            SQLiteDataAdapter ad;
            string Stall_ID = "";
            try
            {
                sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = "select Stall_ID from tblInstallInfo where reg_user_id='" + Reg_User_ID + "' and sync='THIS'";  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource

                if (dt.Rows.Count == 1)
                {
                    Stall_ID = dt.Rows[0]["Stall_ID"].ToString();
                }
                sqlite.Close();

            }
            catch (SQLiteException ex)
            {
                throw ex;
                //Add your exception code here.
                //MessageBox.Show(ex.Message);
            }

            return Stall_ID;
        }

        //-------------Method Added By Sumit on 30-Dec-18 for GSP-983
        public static string Get_Max_Server_User_ID(string Reg_User_ID, string Stall_ID)
        {
            SQLiteConnection sqlite;
            DataTable dt = new DataTable();
            SQLiteDataAdapter ad;
            string Server_User_ID = "";
            try
            {
                sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = "select Max(server_User_ID) from PatientDetails where reg_user_id='" + Reg_User_ID + "' and Stall_ID='" + Stall_ID + "'";  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
                sqlite.Close();
                if (dt.Rows == null || dt.Rows.Count == 0)
                {
                    Server_User_ID = "0";//means no record exist for this PC info in PatientDetails table 
                }
                else if (dt.Rows.Count == 1)
                {
                    if (dt.Rows[0][0] == null || dt.Rows[0][0].ToString() == "")//if no data available
                    {
                        Server_User_ID = "0";
                    }
                    else
                    {
                        Server_User_ID = dt.Rows[0][0].ToString();
                    }
                }


            }
            catch (SQLiteException ex)
            {
                throw ex;
                //Add your exception code here.
                //MessageBox.Show(ex.Message);
            }

            return Server_User_ID;
        }

        public static DataTable Get_Records_To_Update(string Reg_User_ID, string Stall_ID)
        {
            SQLiteConnection sqlite;
            DataTable dt = new DataTable();
            SQLiteDataAdapter ad;

            try
            {
                sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = "select * from PatientDetails where reg_user_id='" + Reg_User_ID + "' and Stall_ID='" + Stall_ID + "' order by server_user_id ASC";  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
                sqlite.Close();

            }
            catch (SQLiteException ex)
            {
                throw ex;
            }

            return dt;
        }

        //-------------Method Added By Sumit on 09-Jan-19 for GSP-1000
        public static bool IsOtherPCData(string uniqueid)
        {
            bool isForeign = false;
            SQLiteConnection sqlite;
            DataTable dt = new DataTable();
            SQLiteDataAdapter ad;

            try
            {
                sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = "select owners_unique_id from PatientDetails where uniqueid=" + uniqueid + ";";  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
                if (dt.Rows.Count == 1)
                {
                    if (dt.Rows[0][0].ToString().Trim() == "-1")
                    {
                        isForeign = false;
                    }
                    else
                    {
                        isForeign = true;
                    }
                }
                sqlite.Close();

            }
            catch (SQLiteException ex)
            {
                throw ex;
                //Add your exception code here.
                //MessageBox.Show(ex.Message);
            }

            return isForeign;
        }
    }
}
