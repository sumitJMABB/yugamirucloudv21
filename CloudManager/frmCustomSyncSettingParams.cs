using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace CloudManager
{
    public partial class frmCustomSyncSettingParams : Form
    {
        public frmCustomSyncSettingParams()
        {
            InitializeComponent();
        }

        public static string sCustomSetting { get; set; }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, string> KeyConfig = new Dictionary<string, string>();

                KeyConfig.Add("If_Application_Is_Idle_For_More_Than_Minute", chkIdleFor.Checked ? "TRUE_" + txtIdleMinutes.Text : "FALSE_00");
                KeyConfig.Add("Between_From_To_Date", chkBetweenTime.Checked ? "TRUE_" + cbFromHours.SelectedItem.ToString()+cbFromMinutes.SelectedItem.ToString() + "_" + cbToHours.SelectedItem.ToString() + cbToMinutes.SelectedItem.ToString() : "FALSE_0000_0000");
                KeyConfig.Add("Closing_Application", chkWhenClosingApp.Checked ? "TRUE" : "FALSE");
                KeyConfig.Add("Starting_Application", chkWhenLaunchApp.Checked ? "TRUE" : "FALSE");

                string qry = "";
                foreach (KeyValuePair<string, string> column in KeyConfig)
                {
                    qry += "update KeyConfig set User1='" + column.Value + "' where KeyName='" + column.Key.ToString() + "';";
                }
                sCustomSetting = qry;
                this.Close();
            }
            catch (Exception ex)
            {
                //Add your exception code here.
                MessageBox.Show(ex.Message);
            }
        }

        private void chkIdleFor_CheckedChanged(object sender, EventArgs e)
        {
            if(chkIdleFor.Checked)
            {
                txtIdleMinutes.Enabled = true;
            }
            else
            {
                txtIdleMinutes.Enabled = false;
            }
        }

        private void frmCustomSyncSettingParams_Load(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, string> DictActiveStatus = CloudUtility.SetCloudSetting();
                string[] sIdleFor = DictActiveStatus["If_Application_Is_Idle_For_More_Than_Minute"].ToString().Split('_');
                string[] sBetweenTime = DictActiveStatus["Between_From_To_Date"].ToString().Split('_');

                chkIdleFor.Checked = Convert.ToBoolean(sIdleFor[0]);
                txtIdleMinutes.Text = sIdleFor[1];
                chkBetweenTime.Checked = Convert.ToBoolean(sBetweenTime[0]);
                cbFromHours.SelectedItem = sBetweenTime[1].ToCharArray()[0].ToString() + sBetweenTime[1].ToCharArray()[1].ToString();
                cbFromMinutes.SelectedItem = sBetweenTime[1].ToCharArray()[2].ToString() + sBetweenTime[1].ToCharArray()[3].ToString();
                cbToHours.SelectedItem = sBetweenTime[2].ToCharArray()[0].ToString() + sBetweenTime[2].ToCharArray()[1].ToString();
                cbToMinutes.SelectedItem = sBetweenTime[2].ToCharArray()[2].ToString() + sBetweenTime[2].ToCharArray()[3].ToString();
                
                chkWhenClosingApp.Checked = Convert.ToBoolean(DictActiveStatus["Closing_Application"].ToString());
                chkWhenLaunchApp.Checked = Convert.ToBoolean(DictActiveStatus["Starting_Application"].ToString());
            }
            catch { }
        }

        private void chkBetweenTime_CheckedChanged(object sender, EventArgs e)
        {
            if(chkBetweenTime.Checked)
            {
                cbFromHours.Enabled = true;
                cbFromMinutes.Enabled = true;
                cbToHours.Enabled = true;
                cbToMinutes.Enabled = true;
            }
            else
            {
                cbFromHours.Enabled = false;
                cbFromMinutes.Enabled = false;
                cbToHours.Enabled = false;
                cbToMinutes.Enabled = false;
            }
        }
    }
}
